import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/:space/:country/:state',
      name: 'home',
      component: Home,
    },
    {
      path: '/:space/:country',
      name: 'home',
      component: Home,
    },
    {
      path: '/:space',
      name: 'home',
      component: Home,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
    },
  ],
});
