// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import io from 'socket.io-client';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import App from './App';
import router from './router';
import config from './config.json';

Vue.config.productionTip = false;

Vue.prototype.$socket = io(`${config.backendUrl}`, { path: '/ws' });
Vue.prototype.$config = config;

Vue.prototype.$api = axios.create({
  baseURL: config.backendUrl,
});

Vue.prototype.$url = config.backendUrl;
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
