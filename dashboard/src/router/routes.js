import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import Notifications from "@/pages/Notifications.vue";
import Icons from "@/pages/Icons.vue";
import Location from "@/pages/Location.vue";
import Login from "@/pages/Login.vue";
import Rss from "@/pages/Rss.vue";
import News from "@/pages/News.vue";

import store from '../store';

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticate) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticate) {
    next()
    return
  }
  next('/login')
}

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    beforeEnter: ifAuthenticated,
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,

      },
      {
        path: "settings",
        name: "settings",
        component: UserProfile
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications
      },
      {
        path: "icons",
        name: "icons",
        component: Icons
      },
      {
        path: "location",
        name: "location",
        component: Location
      },
      {
        path: "rss",
        name: "rss",
        component: Rss
      },
      {
        path: "news",
        name: "news",
        component: News
      },
    ]
  },
  {
    path: "/login",
    name: "login",
    component: Login,
     beforeEnter: ifNotAuthenticated,
  },
  { path: "*", component: NotFound }

];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
