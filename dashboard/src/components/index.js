import FormGroupInput from "./Inputs/formGroupInput.vue";

import DropDown from "./Dropdown.vue";
import LocationTable from "./LocationTable.vue";
import RssTable from "./RssTable.vue";
import NewsTable from "./NewsTable.vue";
import Button from "./Button";

import Card from "./Cards/Card.vue";
import ChartCard from "./Cards/ChartCard.vue";
import StatsCard from "./Cards/StatsCard.vue";
import LoginCard from "./Cards/LoginCard.vue";
import ModalLoc from "./ModalLoc.vue";
import ModalRss from "./ModalRss.vue";

import SidebarPlugin from "./SidebarPlugin/index";

let components = {
  ModalLoc,
  ModalRss,
  FormGroupInput,
  Card,
  ChartCard,
  StatsCard,
  LocationTable,
  DropDown,
  SidebarPlugin,
  LoginCard,
  RssTable,
  NewsTable
};

export default components;

export {
  ModalLoc,
  ModalRss,
  FormGroupInput,
  Card,
  ChartCard,
  StatsCard,
  LocationTable,
  DropDown,
  Button,
  SidebarPlugin,
  LoginCard,
  RssTable,
  NewsTable
};
