/*

 =========================================================
 * Vue Paper Dashboard - v2.0.0
 =========================================================

 * Product Page: http://www.creative-tim.com/product/paper-dashboard
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE.md)

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
import Vue from "vue";
import App from "./App";
import router from "./router/index";
import store from "./store";
import axios from 'axios'
import config from './config.json'
import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

Vue.prototype.$api = axios.create({
    baseURL: config.url,
  });


Vue.use(PaperDashboard);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

const token = localStorage.getItem('token')
if (token) {
      Vue.prototype.$api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}
/* eslint-disable no-new */
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
