var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var config = require('./config.json');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
const channels = require('./boot/channels');
// DB connect
var mongoose = require('mongoose');
mongoose.connect(config.db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}, (err) => {
  if (err) throw err;
  console.log('Successfully connected');
});

var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride('_method'));

// Configuring Passport
var passport = require('passport');
var expressSession = require('express-session');
app.use(passport.initialize());

// Initialize Passport
require('./boot/passport')(passport);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/public', express.static(path.join(__dirname, 'public')));

app.io = require('socket.io')({ path: '/ws' });

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dashboardRouter = require('./routes/dashboard');
var authRouter = require('./routes/auth')(passport);

const allowCrossDomain = require('./middleware/allowCrossDomain');

require('./middleware/ws')(app.io);

var wsLiveRouter = require('./routes/wsLive')(app.io);
var wsSocialRouter = require('./routes/wsSocial')(app.io);

app.use(allowCrossDomain);
app.use('/', indexRouter);
app.use('/', authRouter);
app.use('/users',  usersRouter);
app.use('/dashboard', dashboardRouter);
app.use('/wsLive', wsLiveRouter);
app.use('/wsSocial', wsSocialRouter);
channels.init();
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
