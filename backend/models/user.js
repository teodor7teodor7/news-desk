const Mongoose = require('mongoose');

const userSchema = new Mongoose.Schema({
  _id: Mongoose.Schema.Types.ObjectId,
  username: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
  },
  token: {
    type: String,
    unique: true,
  },
  date: Date,
});
module.exports = Mongoose.model('User', userSchema);
