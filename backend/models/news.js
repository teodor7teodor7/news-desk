const Mongoose = require('mongoose');

const news = new Mongoose.Schema({
  _id: Mongoose.Types.ObjectId,
  title: {
    type: String,
    required: true,
    trim: true,
    minlength: 5,
    maxlength: 255,
  },
  rssTitle: {
    type: String,
    trim: true,
    required: true,
    minlength: 4,
    maxlength: 255,
  },
  link: {
    type: String,
    trim: true,
    unique: true,
    required: true,
    minlength: 5,
    maxlength: 2048,
  },
  content: {
    trim: true,
    type: String,
    sparse: true,
    maxlength: 255,
  },
  location: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  city: {
    type: String,
    minlength: 2,
    maxlength: 255,
  },
  state: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  country: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  shareCount: {
    type: Number,
  },
  pubDate: Date,
  date: Date,
  lastVisit: Date,
});

module.exports = Mongoose.model('news', news);
