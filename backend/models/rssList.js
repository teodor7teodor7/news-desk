const Mongoose = require('mongoose');

const rssLists = new Mongoose.Schema({
  _id: Mongoose.Types.ObjectId,
  title: {
    type: String,
    unique: true,
    minlength: 3,
    maxlength: 255,
  },
  url: {
    type: String,
    required: true,
    unique: true,
    minlength: 5,
    maxlength: 255,
  },
  location: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  city: {
    type: String,
    minlength: 2,
    maxlength: 255,
  },
  state: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  country: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  date: Date,
  lastVisit: Date,
});
module.exports = Mongoose.model('RssLists', rssLists);
