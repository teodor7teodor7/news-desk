const Mongoose = require('mongoose');

const location = new Mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
    minlength: 2,
    maxlength: 255,
  },
  name: {
    type: String,
    required: true,
    unique: true,
    minlength: 2,
    maxlength: 255,
  },
  parent: {
    type: String,
    minlength: 2,
    maxlength: 255,
  },
  date: Date,
});
module.exports = Mongoose.model('Location', location);
