const { check, validationResult } = require('express-validator/check');
const Mongoose = require('mongoose');
const RssList = require('../models/rssList');
const News = require('../models/news');
const DEFAULT_PAGE_LIMIT = 10;
const DEFAULT_NEWS_LIMIT = 100;
var moment = require('moment');

exports.newsList = async (req, res) => {
  const page =  parseInt(req.query.page - 1) || 0;
  const limit = parseInt(req.query.limit) || DEFAULT_PAGE_LIMIT;
  const sort = req.query.sort || { _id: -1 };
  News.find({})
    .skip(page * limit)
    .sort(sort)
    .limit(limit)
    .exec((err, news) => {
      if (err) { return res.status(200).json({ err: [{ param: 'system', msg: 'Error query' }] }); }

      News.countDocuments({}).exec((errCount, count) => {
        if (err) {
          return res.json(errCount);
        }
        return res.status(200).json({
          total: count,
          page,
          pageSize: news.length,
          limit,
          news,
        });
      });
    });
};


exports.newsClient = async (req, res) => {
  const page =  parseInt(req.query.page - 1) || 0;
  const limit = parseInt(req.query.limit) || DEFAULT_NEWS_LIMIT;
  const sort = req.query.sort || { pubDate: -1 };


  await News.find({})
    .skip(page * limit)
    .sort(sort)
    .limit(limit)
    .exec((err, news) => {
      if (err) { return res.status(200).json({ err: [{ param: 'system', msg: 'Error query' }] }); }
      News.countDocuments({}).exec((errCount, count) => {
        if (err) {
          return res.json(errCount);
        }
        return res.status(200).json({
          total: count,
          page,
          pageSize: news.length,
          limit,
          news,
        });
      });
    });
};

exports.wsClientLive = async function (location) {
  const sort = { pubDate: -1 };
  const limit = DEFAULT_NEWS_LIMIT;
  const query =  location === 'undefined'  ? {} : { $or: [ { city: location}, {state: location}, {country: location }]};
  const news = await News.find(query).sort(sort).limit(limit).exec();
  //const count = News.countDocuments({}).exec();
  return { news };
};

exports.wsClientSocial = async function (location) {
  var today = moment().startOf('day').toISOString();
  var tomorrow = moment(today).endOf('day').toISOString();

console.log(`today ${today}`)
  const sort = { shareCount: -1 };
  const limit = DEFAULT_NEWS_LIMIT;
  const query =  location === 'undefined'  
                ? { pubDate: { $gte: today, $lte: tomorrow } } 
                : { $and: [ { pubDate: { $gte: today, $lte: tomorrow } }, { $or: [ { city: location}, {state: location}, {country: location }]}]};
  const news = await News.find(query).sort(sort).limit(limit).exec();
  return { news };
};
