const { check, validationResult } = require('express-validator/check');
const Fs = require('fs');
const Mongoose = require('mongoose');
const Axios = require('axios');
const Parser = require('rss-parser');
const Path = require('path');
const im = require("imagemagick");
const webIconScraper = require('web-icon-scraper');
const RssList = require('../models/rssList');
const Location = require('../models/location');
const crypto = require("crypto");
const imageType = require('image-type');

const DEFAULT_PAGE_LIMIT = 10;

exports.rssList = (req, res) => {
  const page =  parseInt(req.query.page - 1) || 0;
  const limit = parseInt(req.query.limit) || DEFAULT_PAGE_LIMIT;
  const sort = req.query.sort || { _id: -1 };

  RssList.find({})
    .skip(page * limit)
    .sort(sort)
    .limit(limit)
    .exec((err, rss) => {
      if (err) {  return res.status(200).json({ err: [{ param: 'system', msg: 'Error query' }] }); }
      RssList.countDocuments({}).exec((errCount, count) => {
        if (err) {
          return res.json(errCount);
        }
        return res.status(200).json({
          total: count,
          page,
          pageSize: rss.length,
          limit,
          rss,
        });
      });
    });
};
exports.readRssChannel = (req, res) => {
  RssList.findOne({ _id: req.params.id },
    (err, rss) => {
      if (err) { return res.status(200).json({ msg: err }); }
      return res.status(200).json(rss);
    });
};
exports.createRssChannel = [
  check('title').isLength({ min: 3, max: 255 }).trim().withMessage('Title min 3, max 22'),
  check('url').isLength({ min: 3, max: 255 }).trim().withMessage('Url min 3, max 255'),
  check('location').isLength({ min: 2, max: 255 }).trim().withMessage('Location min 2, max 255'),

  async (req, res) => {
    const errorFormatter = ({ msg, param }) => ({ param, msg });
    const errors = validationResult(req).formatWith(errorFormatter);

try {
      validationResult(req).throw();
    } catch (err) { return res.status(200).json({ err: errors.array() }); }

      const parser = new Parser();
      const feed = await parser.parseURL(req.body.url);
      const path = Path.resolve(__dirname, '../public/img/logo/', `${String(req.body.title)}`);

      try {
        await webIconScraper({
          url: feed.link,
          sort: 'asc',
          limit: 1,
          checkStatus: true,
          followRedirectsCount: 1,
        }).then(async (output) => {
          const url = String(output.icons[0].link);
          const response = await Axios({
            url,
            method: 'GET',
            responseType: 'arraybuffer',
          });

          const buf = Buffer.from(response.data);
          const it = imageType(buf);
          if (it === null) { throw new Error('Error save rss image'); }
          const { ext } = it;
          await im.resize({
            srcData: buf,
            srcFormat: ext,
            width: 16,
            height: 16,
            format: 'png',
            progressive: false,
          },
          (err, stdout) => {
            if (err) { throw err; }
            Fs.writeFileSync(path, stdout, 'binary');
          });
        });
      } catch (err) {
        return res.status(200).json({ err: [{ param: 'system', msg: 'Error save image' }] });
      }
      const locHierarchy = await locationHierarchy(req.body.location);
      console.log(locHierarchy);
      const rssList = new RssList({
        _id: new Mongoose.Types.ObjectId(),
        title: req.body.title,
        url: req.body.url,
        location: req.body.location,
        date: new Date(),
        city: locHierarchy[2],
        state: locHierarchy[1],
        country: locHierarchy[0]
      });
      rssList.save((err) => {
        if (err) {
          return res.status(200).json({ err: [{ param: 'system', msg: 'Error save rss channel' }] });
        }
        return res.status(200).json({ msg: 'success' });
      });
  },
];

exports.updateRssChannel = [
  check('title').isLength({ min: 3, max: 22 }).trim().withMessage('Title min 3, max 22'),
  check('url').isLength({ min: 3, max: 255 }).trim().withMessage('Url min 3, max 255'),
  check('location').isLength({ min: 2, max: 255 }).trim().withMessage('Location min 2, max 255'),

async (req, res) => {
    const errorFormatter = ({ msg, param }) => ({ param, msg });
    const errors = validationResult(req).formatWith(errorFormatter);
    try {
      const locHierarchy = await locationHierarchy(req.body.location);
      const city = locHierarchy[2];
      const state = locHierarchy[1];
      const country = locHierarchy[0];
      validationResult(req).throw();
      RssList.findByIdAndUpdate(req.params.id, { $set: { title: req.body.title, url: req.body.url, location: req.body.location, city, state, country } },
        (err) => {
          if (err) {
            return res.status(200).json({ err: [{ param: 'system', msg: 'Error save rss channel' }] });
          }
          return res.status(200).json({ msg: 'success' });
        });
    } catch (err) { return res.status(200).json({ err: errors.array() }); }
  },
];
exports.deleteRssChannel = (req, res) => {
  RssList.deleteOne({ _id: req.params.id }, (err) => {
    if (err) { return res.status(200).json({ err: [{ param: 'system', msg: 'Error delete rss channel' }] }); }
    return res.status(200).json({ msg: 'success' });
  });
};
async function locationHierarchy(id, location = []) {
  let res = await Location.findOne({id},(err, loc) => {
    if (err) {}
    if (loc) { return loc }
  });
  if(res.parent == 'undefined') {
    return location.concat(res.id,[],[])
  } 
  return location.concat(await locationHierarchy(res.parent, location), res.id) 
  
}