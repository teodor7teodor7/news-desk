const { check, validationResult } = require('express-validator/check');
const Mongoose = require('mongoose');
const Location = require('../models/location');

const DEFAULT_PAGE_LIMIT = 10;

exports.parentLocation = (req, res) => {
  const parent = req.params.parent || 'undefined';
  const sort = req.query.sort || { id: 1 };
  Location.find({ parent })
    .sort(sort)
    .exec((err, loc) => {
      if (err) { return res.status(200).json({ err: [{ param: 'system', msg: 'Error query' }] }); }

      return res.status(200).json({
        loc,
      });
    });
};

exports.locationList = (req, res) => {
  const page = parseInt(req.query.page - 1) || 0;
  const limit = parseInt(req.query.limit) || DEFAULT_PAGE_LIMIT;
  const sort = req.query.sort || { _id: -1 };

  Location.find({})
    .skip(page * limit)
    .sort(sort)
    .limit(limit)
    .exec((err, loc) => {
      if (err) { return res.status(200).json({ err: [{ param: 'system', msg: 'Error query' }] }); }

      Location.countDocuments({}).exec((errCount, count) => {
        if (err) {
          return res.json(errCount);
        }
        return res.status(200).json({
          total: count,
          page,
          pageSize: loc.length,
          limit,
          loc,
        });
      });
    });
};

exports.locationId = (req, res) => {
  Location.find({}).exec((err, loc) => {
    if (err) { return res.status(200).json({ err: [{ param: 'system', msg: 'Error query' }] }); }
    return res.status(200).json({
      loc,
    });
  });
};

exports.readLocation = (req, res) => {
  Location.findOne({ id: req.params.id },
    (err, loc) => {
      if (err) { return res.status(200).json({ msg: err }); }
      return res.status(200).json(loc);
    });
};
exports.createLocation = [
  check('id').isLength({ min: 2, max: 22 }).trim().withMessage('Id min 2, max 22'),
  check('name').isLength({ min: 2, max: 255 }).trim().withMessage('Name min 2, max 22'),
  check('parent').isLength({ min: 2, max: 255 }).trim().withMessage('Parent min 2, max 22'),
  (req, res) => {
    const errorFormatter = ({ msg, param }) => ({ param, msg });
    const errors = validationResult(req).formatWith(errorFormatter);
    try {
      validationResult(req).throw();
      const location = new Location({
        id: req.body.id,
        name: req.body.name,
        parent: req.body.parent,
        date: new Date(),
      });
      location.save((err) => {
        if (err) { return res.status(200).json({ err: errors.array() }); }
        return res.status(200).json({ msg: 'success' });
      });
    } catch (err) { return res.status(200).json({ err: errors.array() }); }
  },

];

exports.updateLocation = [
  check('id').isLength({ min: 2, max: 22 }).trim().withMessage('Id min 2, max 22'),
  check('name').isLength({ min: 2, max: 255 }).trim().withMessage('Name min 2, max 22'),
  check('parent').isLength({ min: 2, max: 255 }).trim().withMessage('Parent min 2, max 22'),
(req, res) => {
    const errorFormatter = ({ msg, param }) => ({ param, msg });
    const errors = validationResult(req).formatWith(errorFormatter);
    try {
      validationResult(req).throw();
      Location.updateOne({ id: req.params.id },
        { $set: { name: req.body.name, parent: req.body.parent } },
        { new: true },
        (err) => {
          if (err) { return res.status(200).json({ msg: err }); }
          return res.status(200).json({ msg: 'success' });
        });
    } catch (err) { return res.status(200).json({ err: errors.array() }); }
  },
];
exports.deleteLocation = (req, res) => {
  console.log(req.params.id);
  Location.remove({ id: req.params.id }, (err) => {
    if (err) { return res.status(200).json({ msg: ' error' }); }
    return res.status(200).json({ msg: 'success' });
  });
};
