const bCrypt = require('bcrypt-nodejs');
const { check, validationResult } = require('express-validator/check');
const User = require('../models/user');

exports.settings = (req, res) => {
  User.findOne({ _id: req.user._id }, (err, user) => {
    if (err) { res.status(401); return false;}

    res.json({
      title: 'News desk',
      username: user.username,
      email: user.email,
    });
  });
};

exports.changeProfile = [
  check('username').isLength({ min: 5, max: 22 }).trim(),
  check('email').isEmail().trim(),
  check('password').isLength({ min: 5, max: 22 }).trim(),
  check('password').custom((value, { req }) => User.findOne({ _id: req.user._id }).then((user) => {
    if (!user) { return Promise.reject('Error user'); }
    if (!bCrypt.compareSync(value, user.password)) { return Promise.reject('Error password'); }
  })),
  (req, res) => {
    const errorFormatter = ({ msg, param }) => ({ param, msg });
    const { username, email, password } = req.body;
    const errors = validationResult(req).formatWith(errorFormatter);
    try {
      validationResult(req).throw();
      User.findOne({ _id: req.user._id }, (error, user) => {
        if (error) { validationResult(req).throw(); }
        if (!user) { return res.status(404).json({ err: [{ param: 'username', msg: 'Error user' }] }); }
        if (!bCrypt.compareSync(password, user.password)) { return res.status(404).json({ err: [{ param: 'password', msg: 'Error password' }] }); }
        user.set({ username, email });
        user.save((err) => {
          if (err) { return res.status(404).json({ err: [{ param: 'system', msg: 'Error save sata' }] }); }
          return res.status(200).json({ status: true, username });
        });
      });
    } catch (err) { return res.status(404).json({ err: errors.array() }); }
  },
];

exports.changePassword = [
  check('password').isLength({ min: 5, max: 22 }).trim().withMessage('Password min 5, max 22'),
  check('password').custom((value, { req }) => User.findById(req.user._id ).then((user) => {
    if (!user) { return Promise.reject('Error password'); }
    if (!bCrypt.compareSync(value, user.password)) { return Promise.reject('Error password'); }
  })),
  check('newPassword').isLength({ min: 5, max: 22 }).trim().withMessage('Password min 5, max 22'),
  check('newPassword').custom((value, { req, res }) => {
    if (value !== req.body.repeatPassword) {
      return res.status(404).json({ err: [{ param: 'system', msg: 'Password confirmation does not match password' }] });
    } return true;
  }),
(req, res) => {
    const id = req.user._id;
    const newPassword = bCrypt.hashSync(req.body.newPassword, bCrypt.genSaltSync(10), null);
    const errorFormatter = ({ msg, param }) => ({ param, msg });
    const errors = validationResult(req).formatWith(errorFormatter);
    try {
      validationResult(req).throw();
      User.findById(id, (err, user) => {
        if (err) { validationResult(req).throw(); }
        if (!user) {
          return res.status(404).json({ err: [{ param: 'system', msg: 'Password confirmation does not match password' }] });
        }
        user.set({ password: newPassword });
        user.save((error) => {
          if (error) {
            return res.status(404).json({ err: [{ param: 'system', msg: 'Password confirmation does not match password' }] });
          }
        });
      });
      return res.status(200).json({
        status: true,
      });
    } catch (err) { return res.status(404).json({ err: errors.array() }); }
  },
];
