'use strict';
const Mongoose = require('mongoose');
const Location = require('../models/location');
var dbm;
var type;
var seed;
const USAloc = require('./USAloc.json');
/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  *
  // var ar = $(".flagicon").closest("tr");
  // let d =[];for(let a = 0; a < (ar.length-1); a++){ d.push({id: ar[a].cells[1].innerText.trim(),
  //    name: ar[a].cells[0].innerText.trim(), parent:'US'});}
  *
  var ar = $(".flagicon").closest("tr");
  let d =[];for(let a = 0; a <= (ar.length-1); a++){ d.push({id: ar[a].cells[1].innerText.trim(),
     name: ar[a].cells[1].innerText.trim(), parent:ar[a].cells[2].innerText.trim()});}
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createCollection('locations', (err) => {
    if (err) throw err;
     console.log('Successfully create collection location');
    //  console.log(USAloc);
      USAloc.forEach((loc) => {
        db.insert('locations', new Location({
            id: loc.id.replace(" ", "-"),
            name: loc.name,
            parent: loc.parent.replace(" ", "-"),
            date: new Date()
          }), (err) => {
            if (err) throw err;
             console.log(`Successfully add ${loc.id} in collection location`);
        });
      });


  })
};

exports.down = function(db) {
  db.dropCollection('locations',  (err) => {
    if (err) throw err;
     console.log('Successfully drop collection location');
  });
};

exports._meta = {
  "version": 1
};
