// db-migrate up --config config.json -e "db"
// db-migrate create add-rss --config config.json -e "db"
'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createCollection('rssLists', (err) => {
    if (err) throw err;
    console.log('Successfully create collection rssList');
  });
};

exports.down = function(db) {
  db.dropCollection('rssLists',  (err) => {
    if (err) throw err;
     console.log('Successfully drop collection rssList');
  });
};

exports._meta = {
  "version": 1
};
