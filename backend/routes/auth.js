const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const User = require('../models/user');

const router = express.Router();

module.exports = (passport) => {
  router.post('/login', (req, res) => {
    passport.authenticate('local', { session: false }, (err, user, message) => {
      if (err || !user) {
        return res.status(401).json({ message: 'Something is not right' });
      }
      const token = jwt.sign({
        auth: 'newsdesk',
        agent: req.headers['user-agent'],
        exp: Math.floor(new Date().getTime() / 1000) + 7 * 2 * 60 * 60,
        user: { _id: user._id, email: user.email }
      },
      config.secretOrKey);

      User.findById(user._id, (error, user) => {
        user.set({ token });
        user.save((err) => {
          if (err) { return res.status(401).json({ message: 'Something is not right' }); }
        });
      });
      return res.json({ token });
    })(req, res);
  });

  router.get('/logout', (req, res) => {
    req.logout();
    res.send({});
  });

  return router;
};
