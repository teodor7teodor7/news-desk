const express = require('express');
const newsController = require('../controllers/newsController');
const channels = require('../boot/channels').channels;

const router = express.Router();
module.exports = (io) => {
  const social = io.of('/social');
  router.get('/', async (req, res) => {
    console.log('broadcast social');
    for(let channel in channels.social) {
      if(channels.social[channel] > 0) {
        console.log(`channel social - ${channel}`)
        social.in(channel).emit('news', await newsController.wsClientSocial(channel));
        }
      }
      res.send('response');
  });
  return router;
};
