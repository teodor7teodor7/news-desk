const express = require('express');
const newsController = require('../controllers/newsController');
const locationController = require('../controllers/locationController');

const router = express.Router();

router.get('/', newsController.newsClient);

router.get('/parent-location', locationController.parentLocation);
router.get('/parent-location/:parent', locationController.parentLocation);
module.exports = router;
