const express = require('express');
const isAuthenticated = require('../middleware/isAuthenticated');
const rssListController = require('../controllers/rssListController');
const locationController = require('../controllers/locationController');
const newsController = require('../controllers/newsController');

const router = express.Router();

/* GET home page. */
router.get('/', isAuthenticated, (req, res) => res.json({ status: 'success' }));
router.get('/rss-list', isAuthenticated, rssListController.rssList);
router.get('/rss-list/:id', isAuthenticated, rssListController.readRssChannel);
router.post('/rss-list', isAuthenticated, rssListController.createRssChannel);
router.put('/rss-list/:id', isAuthenticated, rssListController.updateRssChannel);
router.delete('/rss-list/:id', isAuthenticated, rssListController.deleteRssChannel);

router.get('/location', isAuthenticated, locationController.locationList);
router.post('/location', isAuthenticated, locationController.createLocation);
router.put('/location/:id', isAuthenticated, locationController.updateLocation);
router.delete('/location/:id', isAuthenticated, locationController.deleteLocation);


router.get('/location-id', isAuthenticated, locationController.locationId);

router.get('/news', isAuthenticated, newsController.newsList);
module.exports = router;
