const express = require('express');
const newsController = require('../controllers/newsController');
const channels = require('../boot/channels').channels;

const router = express.Router();
module.exports = (io) => {
  const live = io.of('/live');
  router.get('/', async (req, res) => {
    console.log('broadcast live');
    for (const channel in channels.live) {
      if (channels.live[channel] > 0) {
        console.log(`channel live - ${channel}`);
        live.in(channel).emit('news', await newsController.wsClientLive(channel));
        }
      }
      res.send('response');
  });
  return router;
};
