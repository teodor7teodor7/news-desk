const express = require('express');
const isAuthenticated = require('../middleware/isAuthenticated');
const userController = require('../controllers/userController');

const router = express.Router();

/* GET users listing. */
router.get('/settings', isAuthenticated, userController.settings);

/* POST change profile. */
router.post('/settings', isAuthenticated, userController.changeProfile);

/* POST change password. */
router.post('/password', isAuthenticated, userController.changePassword);

module.exports = router;
