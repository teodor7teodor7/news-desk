const LocalStrategy = require('passport-local').Strategy;
const bCrypt = require('bcrypt-nodejs');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const User = require('../models/user');
const config = require('../config.json');

module.exports = (passport) => {
  const isValidPassword = (user, password) => bCrypt.compareSync(password, user.password);
console.log('Pasport init')
  passport.use(new LocalStrategy(
    (username, password, cb) => {

      User.findOne({ username },
        (err, user) => {
          if (err) return cb(err);
          if (!user) return cb(null, false, { message: 'User Not found.' });
          if (!isValidPassword(user, password)) {
            return cb(null, false, { message: 'Invalid Password' }); // redirect back to login page
          }
          return cb(null, user, { message: 'Logged In Successfully' });
        }).catch((err) => cb(err));
    },
  ));

  passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.secretOrKey
  },
  (jwtPayload, cb) => {
    return User.find({ _id: jwtPayload.user._id })
      .then((user) => cb(null, user))
      .catch((err) => cb(err));
  }));
};
