const Mongoose = require('mongoose');
const Location = require('../models/location');
let channels = {
    live: {},
    social: {}
 }
module.exports.init = async () => {
    console.log('Init channels')
     try {
        let locs = await Location.find({});
        for (loc of locs) {
            channels.live[loc.id] = 0;
            channels.social[loc.id] = 0;
        }
    } catch (err) {
        console.error(err);
    }
    return channels;
};

module.exports.channels = channels;