const newsController = require('../controllers/newsController');
const channels = require('../boot/channels').channels;

module.exports = (io) => {
  const live = io.of('/live');
  live.on('connect',
    async (socket) => {
      console.log('User has connected to Live');
      socket.on('channel', async (channel) => {
        console.log(channels.live[channel])
        if(channels.live[channel] === undefined) { return; }
        socket.join(channel);
        live.in(channel).emit('news', await newsController.wsClientLive(channel));
        const clients = socket.adapter.rooms[channel];
        const numClients = (typeof clients !== undefined) ? Object.keys(clients.sockets).length : 0;
       // console.log(`${channel} - ${numClients}`);
        channels.live[channel] = numClients;
    //    console.log(channels.live);
      });
      socket.on('leave', async (channel) => {
      if(channels.live[channel] === undefined) { return; }
       const clients = socket.adapter.rooms[channel];
        const numClients = (typeof clients !== undefined) ? Object.keys(clients.sockets).length : 0;
        console.log(`leave ${channel} - ${numClients}`);
        socket.leave(channel);
        channels.live[channel] = numClients;
   //     console.log(channels.live);          
        })
    });
  const social = io.of('/social');
  social.on('connect',
    async (socket) => {
      console.log('User has connected to Social');
      socket.on('channel', async (channel) => {
        console.log(channels.social[channel])
        if(channels.social[channel] === undefined) { return; }
        socket.join(channel);
        social.in(channel).emit('news', await newsController.wsClientSocial(channel));

        const clients = socket.adapter.rooms[channel];
        const numClients = (typeof clients !== undefined) ? Object.keys(clients.sockets).length : 0;
        console.log(`${channel} - ${numClients}`);

        channels.social[channel] = numClients;
   //     console.log(channels.social);
      });
      socket.on('leave', async (channel) => {
        console.log(channels.social[channel])
        if(channels.social[channel] === undefined) { return; }
        socket.leave(channel);

        const clients = socket.adapter.rooms[channel];
        const numClients = (typeof clients !== undefined) ? Object.keys(clients.sockets).length : 0;
        console.log(`leave ${channel} - ${numClients}`);

        channels.social[channel] = numClients;
   //     console.log(channels.social);
      });
    });
};
