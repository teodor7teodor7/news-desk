const Parser = require('rss-parser');
const Mongoose = require('mongoose');
const RssList = require('./models/rssList');
const request = require('request');
const Axios = require('axios');
const News = require('./models/news');
const config = require('./config.json');

const NEWS_LIMIT = 100;
const apiBufferappUrl = 'https://api.bufferapp.com/1/links/shares.json?url=';

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

Mongoose.connect(config.db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
}, (err) => {
  if (err) throw err;
  console.log('Successfully connected');
});

async function run() {
  const parser = new Parser({
    timeout: 100000,
    maxRedirects: 100,
  });
  let shareCount = 0;
  await RssList.find({})
    .sort({ lastVisit: 1 })
    .exec(async (err, rss) => {
      if (err) { console.error(err); }
        for (one of rss) {
         try {
          await RssList.findByIdAndUpdate(one._id, { $set: {  lastVisit: new Date() } },
            (err) => {
              if (err) {
              console.log(err);
            }
        });

    let feed = await parser.parseURL(one.url);
      console.log(`parse ${one.url}`);     
      console.log(` -  ${one.lastVisit}`);
      
      for (item of feed.items) {
      const oneNemws = await News.find({link: item.link}).count();
      if (oneNemws > 0) { continue; }

    //  await delay(1500)
      const response = await Axios({
        url: `${apiBufferappUrl}${item.link}`,
        method: 'GET',
        responseType: 'json'
      });
      if (response && !response.code) {
          console.log(`${item.link} - shares: ${response.data.shares}` )
          shareCount = response.data.shares;
         } else {
          shareCount = 0;
          console.log(`Error Api Bufferapp: ${response.data.code}`);
       }
      const news = await new News({
      _id: new Mongoose.Types.ObjectId(),
      title: item.title,
      link: item.link,
      rssTitle: one.title,
      content: item.content.slice(0,255),
      pubDate: item.pubDate,
      location: one.location,
      city: one.city,
      state: one.state,
      country: one.country,
      shareCount: shareCount,
      date: new Date(),
      lastVisit: new Date(),
    });
 
      await news.save((err) => {
      if (err) {
           console.log(err);
              }
    });
    
          }
        } catch(err){ console.log(err);  console.log(`error parse ${one.url}`); }
      }

//const saveRawNews = await News.find().sort({ pubDate: -1 }).limit(NEWS_LIMIT);
//const saveNews = await Array.from(saveRawNews).map(item => item._id);
//await News.deleteMany({_id: {$nin: saveNews }});


console.log(`finish parse`);
console.log(`${config.url}:${config.port}/wsLive`);
await request(`${config.url}:${config.port}/wsLive`, function (error, response, body) {
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received

});
await delay(55000);
await run();
});


};


run();
