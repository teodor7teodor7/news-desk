
const Mongoose = require('mongoose');
const RssList = require('./models/rssList');
const request = require('request');
const Axios = require('axios');
const News = require('./models/news');
const config = require('./config.json');
var moment = require('moment');

const NEWS_LIMIT = 100;
const apiBufferappUrl = 'https://api.bufferapp.com/1/links/shares.json?url=';

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

Mongoose.connect(config.db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
}, (err) => {
  if (err) throw err;
  console.log('Social-parser successfully connected');
});

async function run() {
  var today = moment().startOf('day').toISOString();
  var tomorrow = moment(today).endOf('day').toISOString();
  
  try {
    const sort = { lastVisit: 1 };
    let news = await News.find({ pubDate: { $gte: today, $lte: tomorrow } }).sort(sort).limit(NEWS_LIMIT);
    
    for (item of news) {
      await delay(1500);
      let shareCount = item.shareCount;
      const oneNemws = await News.find({link: item.link}).count();
      const response = await Axios({
        url: `${apiBufferappUrl}${item.link}`,
        method: 'GET',
        responseType: 'json'
      });
      if (response && !response.code) {
          console.log(`${item.link} - shares: ${response.data.shares} - ${item.lastVisit}` )
          shareCount = response.data.shares;
         } else {
          console.log(`Error Api Bufferapp: ${response.data.code}`);
          }
       await News.findByIdAndUpdate(item._id, { $set: { shareCount: shareCount, lastVisit:  new Date() } }, (err) => { 
         if (err)  { console.log(err) }   });

        }
    } catch(err){ console.error(err);  console.error(`error social parse ${item.title}`); }


console.log(`finish  social parse`);
console.log(`${config.url}:${config.port}/wsSocial`);
await request(`${config.url}:${config.port}/wsSocial`, function (error, response, body) {
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
});
  await run();

}
run();
